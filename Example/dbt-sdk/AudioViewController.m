//
//  AudioViewController.m
//  DBT Test
//
//  Created by Brandon Trebitowski on 9/4/14.
//  Copyright (c) 2014 Treb Studios. All rights reserved.
//

#import "AudioViewController.h"
#import "DBT.h"
#import "DBTBook.h"
#import "DBTMediaLocation.h"
#import "DBTAudioPath.h"
#import <AVFoundation/AVFoundation.h>

@interface AudioViewController ()<AVAudioPlayerDelegate>
@property(nonatomic, strong) AVAudioPlayer *audioPlayer;
@end

@implementation AudioViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = self.book.bookName;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)playAudio:(id)sender {
    
    [DBT getAudioLocation:@"http"
                  success:^(NSArray *audioLocations) {
                      if(audioLocations.count) {
                          DBTMediaLocation *location = audioLocations[0];
                          NSLog(@"location %@", location);
                          NSNumber *chapterNumber = [NSNumber numberWithInteger:[self.chapter integerValue]];
                          [DBT getAudioPathWithDamId:self.book.damId
                                                book:self.book.bookId
                                             chapter:chapterNumber
                                             success:^(NSArray *audioPaths) {
                                                 DBTAudioPath *audioPath = audioPaths[0];
                                                 NSString *urlString = [NSString stringWithFormat:@"%@/%@",location.baseURL,audioPath.path];
                                                 NSURL *url = [NSURL URLWithString:urlString];
                                                 [self playAudioAtURL:url];
                                             } failure:^(NSError *error) {
                                                 NSLog(@"Error: %@", error);
                                             }];
                      }
                  } failure:^(NSError *error) {
                      
                  }];
}

- (void) playAudioAtURL:(NSURL *) url {
    NSError *error;
    NSData *audioData = [[NSData alloc] initWithContentsOfURL:url options:NSDataReadingMappedIfSafe error:&error ];
    
    self.audioPlayer = [[AVAudioPlayer alloc]
                    initWithData:audioData error:&error];
    if (error)
    {
        NSLog(@"Error in audioPlayer: %@",
              [error localizedDescription]);
    } else {
        self.audioPlayer.delegate = self;
        [self.audioPlayer play];
    }
}

-(void)audioPlayerDidFinishPlaying:
(AVAudioPlayer *)player successfully:(BOOL)flag
{
}

-(void)audioPlayerDecodeErrorDidOccur:
(AVAudioPlayer *)player error:(NSError *)error
{
}

-(void)audioPlayerBeginInterruption:(AVAudioPlayer *)player
{
}

-(void)audioPlayerEndInterruption:(AVAudioPlayer *)player
{
}

@end
