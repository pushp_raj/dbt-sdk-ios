//
//  NMTMasterViewController.m
//  DBT Test
//
//  Created by Brandon Trebitowski on 9/4/14.
//  Copyright (c) 2014 Treb Studios. All rights reserved.
//

#import "VolumesTableViewController.h"
#import "BookTableViewController.h"
#import "DBT.h"
#import "DBTVolume.h"

@interface VolumesTableViewController () <UITextFieldDelegate>
@property(nonatomic, strong) NSArray *volumes;
@property(nonatomic, strong) NSString *languageCode;
@property(nonatomic, strong) NSString *mediaType;
@end

@implementation VolumesTableViewController

- (void)awakeFromNib
{
    [super awakeFromNib];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title = @"";
    self.languageCode = @"ENG";
    self.mediaType = @"text";
    [self reload];
}

- (void) reload {
    [DBT getLibraryVolumeWithDamID:nil
               languageCode:self.languageCode
                      media:self.mediaType
                    success:^(NSArray *volumes) {
                        self.volumes = volumes;
                        [self.tableView reloadData];
                        NSLog(@"Volumes %@", volumes);
                    } failure:^(NSError *error) {
                        NSLog(@"Error: %@", error);
                    }];
}

#pragma mark - Table View

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.volumes.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];

    DBTVolume *volume = self.volumes[indexPath.row];
    cell.textLabel.text = [NSString stringWithFormat:@"%@: %@",volume.mediaType, volume.volumeName];
    cell.detailTextLabel.text = volume.collectionName;
    return cell;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"showDetail"]) {
        NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
        DBTVolume *volume = self.volumes[indexPath.row];
        [[segue destinationViewController] setVolume:volume];
    }
}

- (BOOL) textFieldShouldReturn:(UITextField *)textField {
    self.languageCode = textField.text;
    [self reload];
    [textField resignFirstResponder];
    return YES;
}

- (IBAction)segmentedControlValueChanged:(id)sender {
    UISegmentedControl *ctrl = (UISegmentedControl *) sender;
    if (ctrl.selectedSegmentIndex) {
        self.mediaType = @"audio";
    } else {
        self.mediaType = @"text";
    }
    
    [self reload];
}

@end
