//
//  BookTableViewController.h
//  DBT Test
//
//  Created by Brandon Trebitowski on 9/4/14.
//  Copyright (c) 2014 Treb Studios. All rights reserved.
//

#import <UIKit/UIKit.h>

@class DBTVolume;

@interface BookTableViewController : UITableViewController
@property(nonatomic, strong) DBTVolume *volume;
@end
